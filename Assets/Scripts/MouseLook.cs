
using System;
using UnityEngine;



namespace UnityStandardAssets.Characters.FirstPerson
{
    [Serializable]
    public class MouseLook
    {
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public bool clampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool smooth;
        public float smoothTime = 5f;
        int RightFingerId=-1;
       

        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;


        public void Init(Transform character, Transform camera)
        {
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;
        }


        public void LookRotation(Transform character, Transform camera)
        {
            Vector2 direction=Vector2.zero;
            if (Input.touchCount > 0)
            {
                foreach (Touch touch in Input.touches)
                {
                    //For right half screen
                    if (touch.phase == TouchPhase.Began && touch.position.x >= Screen.width / 2 && RightFingerId == -1)
                    {
                        //Store FingerID
                        RightFingerId = touch.fingerId;
                        Debug.Log("Left touch started");
                    }
                    if(touch.phase==TouchPhase.Moved && touch.position.x>=Screen.width/2 &&RightFingerId==touch.fingerId)
                    {
                        direction = touch.deltaPosition.normalized;
                    }
                    if (touch.phase == TouchPhase.Ended)
                    {
                        RightFingerId = -1;

                    }
                }
            }



                    float yRot = direction.x * XSensitivity;
            float xRot = direction.y * YSensitivity;

            m_CharacterTargetRot *= Quaternion.Euler (0f, yRot, 0f);
            m_CameraTargetRot *= Quaternion.Euler (-xRot, 0f, 0f);

            if(clampVerticalRotation)
                m_CameraTargetRot = ClampRotationAroundXAxis (m_CameraTargetRot);

            if(smooth)
            {
                character.localRotation = Quaternion.Slerp (character.localRotation, m_CharacterTargetRot,
                    smoothTime * Time.deltaTime);
                camera.localRotation = Quaternion.Slerp (camera.localRotation, m_CameraTargetRot,
                    smoothTime * Time.deltaTime);
            }
            else
            {
                character.localRotation = m_CharacterTargetRot;
                camera.localRotation = m_CameraTargetRot;
            }
        }


        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

            angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

    }
}
