﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.Characters.FirstPerson;

public class Fire : MonoBehaviour {

    FirstPersonController fpc;
    bool pressed;
    bool clicked;
    void Start()
    {
        fpc = FindObjectOfType<FirstPersonController>();
    }

    void FixedUpdate()
    {
        if (!pressed)
        {
            return;
        }
        else 
        {
            fpc.Shoot();
        }
    }
    public void OnPointerDown()
    {
        
       pressed = true;
     
    }

    public void OnPointerUp()
    {
        pressed = false;
    }
   
    




}
