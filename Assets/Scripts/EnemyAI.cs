﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
public class EnemyAI :LivingEntity {

    public Transform weaponHold;
    public Gun startingGun;
    Gun equippedGun;

    float rotation;
    float time;
    float positionX;
    float positionZ;

   


    public LayerMask layerMask;
    public LayerMask layerMask2;

                    
    private NavMeshAgent pathFinder;                               
    private Transform player;
    private Transform enemyCamera;

    public Vector3 playerLastPosition;
    

    Quaternion lookRotation;
    public bool chasingCompleted;
    public bool chasingStarted;


    void Awake()
    {
        // Setting up the references.
       
        pathFinder = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemyCamera = transform.FindChild("EnemyCamera");
        //playerHealth = player.GetComponent<PlayerHealth>();
      
    }

    protected override void Start()
    {
        base.Start();
        if (startingGun != null)
        {
            EquipGun(startingGun);

        }
       
    }

    public void EquipGun(Gun gunToEquip)
    {
        if (equippedGun != null)
        {
            Destroy(equippedGun.gameObject);
        }
        equippedGun = Instantiate(gunToEquip, weaponHold.position, Quaternion.identity) as Gun;
        equippedGun.transform.parent = weaponHold;
    }

    public void Shoot()
    {
        equippedGun.Shoot();
    }
    void FixedUpdate()
    {
        AIStuffs();
    }

    void AIStuffs()
    {
        RaycastHit hit;
        if (Physics.SphereCast(enemyCamera.transform.position, 10f, transform.forward, out hit, Mathf.Infinity, layerMask))
        {
            Vector3 direction = player.transform.position - transform.position;
            Debug.DrawRay(enemyCamera.transform.position, direction, Color.red);
            direction.Normalize();
            lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.fixedDeltaTime);

            RaycastHit hit2;

            if (Physics.SphereCast(enemyCamera.transform.position, 10f, direction, out hit2, Mathf.Infinity, layerMask2))
            {
                if (hit2.collider.tag == "Player")
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.fixedDeltaTime);
                    equippedGun.transform.rotation = lookRotation;
                    transform.rotation = equippedGun.transform.rotation;
                   // Shoot();
                    playerLastPosition = player.transform.position;

                }
                else if (hit2.collider.tag != "Player")
                {
                    if (Time.time > time)
                    {
                        rotation = Random.Range(0, 360);

                        time = Time.time + Random.Range(3, 7);
                    }
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotation, 0), Time.deltaTime);
                    //transform.position = new Vector3(Mathf.Lerp(transform.position.x, playerLastPosition.x, Time.fixedDeltaTime), transform.position.y, Mathf.Lerp(transform.position.z, playerLastPosition.z, Time.fixedDeltaTime));
                    pathFinder.SetDestination(playerLastPosition);

                }
            }


        }
        else
        {
            if (Time.time > time)
            {
                rotation = Random.Range(0, 360);
                positionX = Random.Range(0, 40);
                positionZ = Random.Range(0, 40);
                time = Time.time + Random.Range(2, 10);
            }
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotation, 0), Time.fixedDeltaTime);

            pathFinder.SetDestination(new Vector3(positionX, transform.position.y, positionZ));

        }
    }
}
