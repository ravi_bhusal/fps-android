﻿using UnityEngine;
using System.Collections;

public class Spawners : MonoBehaviour {

  
    public EnemyAI enemy;
    public float spawnTime = 5f;
    float curSpawnTime;

    

    public Transform[] spawnPoints;
    void Start()
    {
        
    }

    void Update()
    {
        
       
            curSpawnTime -= 1 * Time.deltaTime;          

        
        if (curSpawnTime <= 0)
        {
            Spawn();
            curSpawnTime = spawnTime;

        }

    }
    void Spawn()
    {
        Vector3 point = spawnPoints[Random.Range(0, 4)].transform.position;
        enemy = Instantiate(enemy, point, Quaternion.identity) as EnemyAI;
        
    }
}
