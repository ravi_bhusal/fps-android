﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {

    public Transform weaponHold;
    public Gun startingGun;
    Gun equippedGun;
    GameObject Canvas;
    GameObject abc;
	void Start ()
    {
	   if (startingGun!=null)
        {
            EquipGun(startingGun);
        }
        Canvas = GameObject.Find("Canvas");
	}
	
    public void EquipGun(Gun gunToEquip)
    {
        if(equippedGun!=null)
        {
            Destroy(equippedGun.gameObject);
        }
        equippedGun = Instantiate(gunToEquip, weaponHold.position, Quaternion.identity) as Gun;
        equippedGun.transform.parent = weaponHold;
    }
	
    public void Shoot()
    {
        equippedGun.Shoot();
    }
    void FixedUpdate()
    {
        if(equippedGun.gameObject.tag=="Pistol")
        {
            Canvas.transform.FindChild("NeedsToBePressed").gameObject.SetActive(false);
            Canvas.transform.FindChild("NeedsToBePressed1").gameObject.SetActive(false);
            Canvas.transform.FindChild("NeedsToBeClicked").gameObject.SetActive(true);
            Canvas.transform.FindChild("NeedsToBeClicked1").gameObject.SetActive(true);


        }
        else if (equippedGun.gameObject.tag == "Ak")
        {
            Canvas.transform.FindChild("NeedsToBePressed").gameObject.SetActive(true);
            Canvas.transform.FindChild("NeedsToBePressed1").gameObject.SetActive(true);

            Canvas.transform.FindChild("NeedsToBeClicked").gameObject.SetActive(false);
            Canvas.transform.FindChild("NeedsToBeClicked1").gameObject.SetActive(false);

        }
    }
}
