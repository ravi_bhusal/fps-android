﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    float speed = 10;
    public LayerMask collisionMask;
    float damage = 5;

    float lifetime = 3;
    float skinWidth = .1f;
    void Start ()
    {
        Collider[] initialCollisions = Physics.OverlapSphere(transform.position, .1f, collisionMask);
        if (initialCollisions.Length > 0)
        {
            OnHitObject(initialCollisions[0]);
        }
        Destroy(gameObject, lifetime);
	}


    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }
	
	void Update ()
    {
        float moveDistance = speed * Time.deltaTime;
        transform.Translate(Vector3.forward * moveDistance);
        CheckCollisions(moveDistance);
	}

    void CheckCollisions(float distance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance, collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHit(hit);
        }
    }

    void OnHit(RaycastHit hit)
    {
        IDamageable damageAbleObject = hit.collider.GetComponent<IDamageable>();
        if (damageAbleObject != null)
        {
            damageAbleObject.TakeHit(damage, hit);
        }
        GameObject.Destroy(gameObject);
    }

    void OnHitObject(Collider c)
    {
        IDamageable damageAbleObject = c.GetComponent<IDamageable>();
        if (damageAbleObject != null)
        {
            damageAbleObject.TakeDamage(damage);
        }
        GameObject.Destroy(gameObject);
    }
}
