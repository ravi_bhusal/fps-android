﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class ClickFire : MonoBehaviour {

    FirstPersonController fpc;

    
	void Start()
    {
        fpc = FindObjectOfType<FirstPersonController>();
    }

    void Update()
    {

    }
    public void Shoot()
    {
        fpc.Shoot();
    }
}
